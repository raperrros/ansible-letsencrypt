server {
  listen 80;
  server_name {{ domain_name }};
  server_name www.{{ domain_name }};
  return 301 https://$host$request_uri;
}

server {
  listen 443 ssl http2;
  server_name {{ domain_name }};
  server_name www.{{ domain_name }};
  root /var/www/vhosts/{{ domain_name }}/httpdocs;
  ssl_certificate /etc/nginx/ssl/selfsigned.crt;
  ssl_certificate_key /etc/nginx/ssl/private/selfsigned.key;
  ssl_certificate /etc/letsencrypt/live/{{ domain_name }}/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/{{ domain_name }}/privkey.pem;
  include snippets/ssl-params-dev.conf;
  #include snippets/ssl-params-prod.conf;
  access_log /var/www/vhosts/{{ domain_name }}/logs/access.log;
  error_log /var/www/vhosts/{{ domain_name }}/logs/error.log;

  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
  #add_header Content-Security-Policy "default-src self; base-url https://{{ domain_name }};";
  #add_header Content-Security-Policy "connect-src none;";
  #add_header Content-Security-Policy "frame-ancestors https://{{ domain_name }};";

  client_max_body_size 256m;

  location = /favicon.ico {
    log_not_found off;
    access_log off;
  }

  location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
  }

  location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass php;
    fastcgi_intercept_errors on;
  }

  location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
    expires max;
    log_not_found off;
  }

# pretty urls for wp
#  location / {
#    try_files $uri $uri/ /index.php?$args;
#  }
}
